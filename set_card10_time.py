#!/usr/bin/env python3

import serial
import time

ser = serial.Serial('/dev/ttyACM0', baudrate=115200)

ser.write(b'')
print(ser.read(ser.in_waiting))

ser.write(b'import utime\r\n')
print(ser.read(ser.in_waiting))

current_time = int(time.time())
ser.write('utime.set_unix_time({})\r\n'.format(current_time).encode('ascii'))
print(ser.read(ser.in_waiting))

ser.write(b'')
