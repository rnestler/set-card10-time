# set-card10-time

Little script to set the time of the card10

## Usage

Install pyserial

```bash
# ArchLinux systemwide
pacman -S python-pyserial
# pip
pip install pyserial~=3.4
```

Run the script
```bash
python set_card10_time.py
```
